package ru.t1consulting.vmironova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.dto.model.ProjectDTO;
import ru.t1consulting.vmironova.tm.enumerated.Status;

import java.util.List;

public interface IProjectDTOService {

    @NotNull
    ProjectDTO add(@NotNull ProjectDTO model) throws Exception;

    void clear() throws Exception;

    boolean existsById(@Nullable String id) throws Exception;

    @Nullable
    List<ProjectDTO> findAll() throws Exception;

    @Nullable
    ProjectDTO findOneById(@Nullable String id) throws Exception;

    int count() throws Exception;

    void remove(@Nullable ProjectDTO model) throws Exception;

    void removeById(@Nullable String id) throws Exception;

    @NotNull
    ProjectDTO update(@Nullable ProjectDTO model) throws Exception;

    @NotNull
    ProjectDTO create(@Nullable String name) throws Exception;

    @NotNull
    ProjectDTO create(
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    void updateById(
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    void changeProjectStatusById(
            @Nullable String id,
            @Nullable Status status
    ) throws Exception;

}
