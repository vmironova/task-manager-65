package ru.t1consulting.vmironova.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.enumerated.Status;
import ru.t1consulting.vmironova.tm.model.Project;

import java.util.List;

public interface IProjectService {

    @NotNull
    Project add(@NotNull Project model) throws Exception;

    void clear() throws Exception;

    boolean existsById(@Nullable String id) throws Exception;

    @Nullable
    List<Project> findAll() throws Exception;

    @Nullable
    Project findOneById(@Nullable String id) throws Exception;

    int count() throws Exception;

    void remove(@Nullable Project model) throws Exception;

    void removeById(@Nullable String id) throws Exception;

    void update(@Nullable Project model) throws Exception;

    void changeProjectStatusById(
            @Nullable String id,
            @Nullable Status status
    ) throws Exception;

    @NotNull
    Project create(@Nullable String name) throws Exception;

    @NotNull
    Project create(
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    void updateById(
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

}
