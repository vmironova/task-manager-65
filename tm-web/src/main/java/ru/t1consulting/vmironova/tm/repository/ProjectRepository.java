package ru.t1consulting.vmironova.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1consulting.vmironova.tm.model.Project;

public interface ProjectRepository extends JpaRepository<Project, String> {

}
