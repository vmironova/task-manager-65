package ru.t1consulting.vmironova.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1consulting.vmironova.tm.dto.model.ProjectDTO;

public interface ProjectDTORepository extends JpaRepository<ProjectDTO, String> {

}
