package ru.t1consulting.vmironova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.lang.Nullable;
import ru.t1consulting.vmironova.tm.dto.model.ProjectDTO;

import java.util.List;

public interface IProjectRestEndpoint {

    @Nullable
    List<ProjectDTO> findAll() throws Exception;

    @NotNull
    ProjectDTO save(@NotNull ProjectDTO project) throws Exception;

    @Nullable
    ProjectDTO findById(@NotNull String id) throws Exception;

    boolean existsById(@NotNull String id) throws Exception;

    long count() throws Exception;

    void deleteById(@NotNull String id) throws Exception;

    void delete(@NotNull ProjectDTO project) throws Exception;

    void clear() throws Exception;

}
