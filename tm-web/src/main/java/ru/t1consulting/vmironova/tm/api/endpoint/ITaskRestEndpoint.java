package ru.t1consulting.vmironova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.lang.Nullable;
import ru.t1consulting.vmironova.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskRestEndpoint {

    @Nullable
    List<TaskDTO> findAll() throws Exception;

    @NotNull
    TaskDTO save(@NotNull TaskDTO project) throws Exception;

    @Nullable
    TaskDTO findById(@NotNull String id) throws Exception;

    boolean existsById(@NotNull String id) throws Exception;

    long count() throws Exception;

    void deleteById(@NotNull String id) throws Exception;

    void delete(@NotNull TaskDTO project) throws Exception;

    void clear() throws Exception;

}
