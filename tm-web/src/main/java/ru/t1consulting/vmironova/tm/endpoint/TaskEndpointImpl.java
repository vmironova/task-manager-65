package ru.t1consulting.vmironova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1consulting.vmironova.tm.api.endpoint.ITaskRestEndpoint;
import ru.t1consulting.vmironova.tm.api.service.dto.ITaskDTOService;
import ru.t1consulting.vmironova.tm.dto.model.TaskDTO;

import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TaskEndpointImpl implements ITaskRestEndpoint {

    @Autowired
    private ITaskDTOService taskService;

    @Override
    @GetMapping("/findAll")
    public List<TaskDTO> findAll() throws Exception {
        return taskService.findAll();
    }

    @NotNull
    @Override
    @PostMapping("/save")
    public TaskDTO save(@RequestBody final @NotNull TaskDTO task) throws Exception {
        return taskService.update(task);
    }

    @Nullable
    @Override
    @GetMapping("/findById/{id}")
    public TaskDTO findById(@PathVariable("id") final @NotNull String id) throws Exception {
        return taskService.findOneById(id);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@PathVariable("id") final @NotNull String id) throws Exception {
        return (taskService.findOneById(id) == null);
    }

    @Override
    @GetMapping("/count")
    public long count() throws Exception {
        return taskService.count();
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") @NotNull String id) throws Exception {
        taskService.removeById(id);
    }

    @Override
    @PostMapping("/delete")
    public void delete(@RequestBody @NotNull TaskDTO task) throws Exception {
        taskService.remove(task);
    }

    @Override
    public void clear() throws Exception {
        taskService.clear();
    }

}
